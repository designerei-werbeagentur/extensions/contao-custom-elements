<?php
return array(
    'label' => array('Hintergrundbild'),
    'types' => array('content'),
    'contentCategory' => 'Custom-Elemente',
    'fields' => array(
        'singleSRC' => array(
            'inputType' => 'standardField'
        ),
        'size' => array(
            'inputType' => 'standardField'
        ),
        'overwriteMeta' => array(
            'inputType' => 'standardField'
        ),
        'overlayGroup' => array(
            'label' => array('Overlay-Einstellungen'),
            'inputType' => 'group',
        ),
        'overlayClass' => array(
            'inputType' => 'text',
            'label' => &$GLOBALS['TL_LANG']['tl_content']['overlayClass'],
            'eval' => array('tl_class'=>'w50')
        ),
        'animationGroup' => array(
            'label' => array('Animations-Einstellungen'),
            'inputType' => 'group',
        ),
        'animationType' => array(
            'inputType' => 'standardField',
        ),
        'animationDelay' => array(
            'inputType' => 'standardField',
        ),
        'animationSpeed' => array(
            'inputType' => 'standardField',
        )
    )
);
