<?php
return array(
    'label' => array('Call-To-Action'),
    'types' => array('content'),
    'standardFields' => array('cssID'),
    'contentCategory' => 'Custom-Elemente',
    'fields' => array(
        'textGroup' => array(
            'label' => array('Text-Einstellungen'),
            'inputType' => 'group',
        ),
        'headline' => array(
            'inputType' => 'standardField',
        ),
        'headlineClass' => array(
            'inputType' => 'standardField',
        ),
        'text' => array(
            'inputType' => 'standardField',
            'eval' => array('tl_class'=>'clr', 'mandatory'=>false)
        ),
        'linkGroup' => array(
            'label' => array('Link-Einstellungen'),
            'inputType' => 'group',
        ),
        'url' => array(
            'inputType' => 'standardField'
        ),
        'target' => array(
            'inputType' => 'standardField'
        ),
        'linkTitle' => array(
            'inputType' => 'standardField'
        ),
        'titleText' => array(
            'inputType' => 'standardField'
        ),
        'buttonGroup' => array(
            'label' => array('Button-Einstellungen'),
            'inputType' => 'group',
        ),
        'buttonStyle' => array(
            'inputType' => 'standardField'
        ),
        'buttonClass' => array(
            'inputType' => 'standardField'
        ),
        'fullwidth' => array(
            'inputType' => 'standardField'
        ),
        'invertStyle' => array(
            'inputType' => 'standardField'
        ),
        'animationGroup' => array(
            'label' => array('Animations-Einstellungen'),
            'inputType' => 'group',
        ),
        'animationType' => array(
            'inputType' => 'standardField',
        ),
        'animationDelay' => array(
            'inputType' => 'standardField',
        ),
        'animationSpeed' => array(
            'inputType' => 'standardField',
        )
    )
);
