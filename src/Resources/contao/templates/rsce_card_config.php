<?php
return array(
    'label' => array('Card'),
    'types' => array('content'),
    'standardFields' => array('cssID'),
    'contentCategory' => 'Custom-Elemente',
    'fields' => array(
        'textGroup' => array(
            'label' => array('Text-Einstellungen'),
            'inputType' => 'group',
        ),
        'headline' => array(
            'inputType' => 'standardField',
        ),
        'headlineClass' => array(
            'inputType' => 'standardField',
        ),
        'text' => array(
            'inputType' => 'standardField',
            'eval' => array('tl_class'=>'clr')
        ),
        'backgroundGroup' => array(
            'label' => array('Hintergrund-Einstellungen'),
            'inputType' => 'group',
        ),
        'backgroundClass' => array(
            'inputType' => 'text',
            'label' => &$GLOBALS['TL_LANG']['tl_content']['backgroundClass'],
            'eval' => array('tl_class'=>'w50')
        ),
        'imageGroup' => array(
            'label' => array('Bildeinstellungen'),
            'inputType' => 'group',
        ),
        'addImage' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_content']['addImage'],
            'inputType' => 'checkbox',
        ),
        'singleSRC' => array(
            'inputType' => 'standardField',

            'dependsOn' => [
                'field' => 'addImage'
            ]
        ),
        'size' => array(
            'inputType' => 'standardField',
            'eval' => array('tl_class'=>'w50'),
            'dependsOn' => [
                'field' => 'addImage'
            ]
        ),
        'imgAspect' => array(
            'inputType' => 'standardField',
            'eval' => array('tl_class'=>'w50'),
            'dependsOn' => [
                'field' => 'addImage'
            ]
        ),
        'imgPosition' => array(
            'inputType' => 'select',
            'label' => &$GLOBALS['TL_LANG']['tl_content']['floating'],
            'options' => array('above', 'below', 'background'),
            'reference' => &$GLOBALS['TL_LANG']['tl_content']['imgPosition'],
            'eval' => array('tl_class'=>'w50 clr'),
            'dependsOn' => [
                'field' => 'addImage'
            ]
        ),
        'overwriteMeta' => array(
            'inputType' => 'standardField',
            'eval' => array('tl_class'=>'clr'),
            'dependsOn' => [
                'field' => 'addImage'
            ]
        ),
        'linkGroup' => array(
            'label' => array('Link-Einstellungen'),
            'inputType' => 'group',
        ),
        'addLink' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_content']['addLink'],
            'inputType' => 'checkbox',
        ),
        'url' => array(
            'inputType' => 'standardField',
            'dependsOn' => [
                'field' => 'addLink'
            ]
        ),
        'target' => array(
            'inputType' => 'standardField',
            'dependsOn' => [
                'field' => 'addLink'
            ]
        ),
        'linkTitle' => array(
            'inputType' => 'standardField',
            'dependsOn' => [
                'field' => 'addLink'
            ]
        ),
        'titleText' => array(
            'inputType' => 'standardField',
            'dependsOn' => [
                'field' => 'addLink'
            ]
        ),
        'animationGroup' => array(
            'label' => array('Animations-Einstellungen'),
            'inputType' => 'group',
        ),
        'animationType' => array(
            'inputType' => 'standardField',
        ),
        'animationDelay' => array(
            'inputType' => 'standardField',
        ),
        'animationSpeed' => array(
            'inputType' => 'standardField',
        )
    )
);
