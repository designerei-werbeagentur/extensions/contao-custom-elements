# Contao-Custom-Elements

## Function & usage

This extension for Contao Open Source CMS provides predefined custom-elements. It is based on the contao extension [madeyourday/contao-rocksolid-custom-elements](https://github.com/madeyourday/contao-rocksolid-custom-elements). All templates are extended with additional TailwindCSS classes and components to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit).

## Elements
- background-image
- card
